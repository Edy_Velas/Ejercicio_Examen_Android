package curso.umg.gt.ejercicioprueba;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    private EditText et1, et2,et3,et4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);

    }

    public void login(View view) {

        String nom = et1.getText().toString();
        String ap = et2.getText().toString();
        String pas1 = et3.getText().toString();
        String pas2 = et4.getText().toString();

        if (pas1.equals(pas2)) {
            Intent i = new Intent(this, InteresesActivity.class);
            startActivity(i);

        } else {
            Toast notification = Toast.makeText(this, "Las Contraseñas no coinciden ", Toast.LENGTH_SHORT);
            notification.show();
        }
    }
}
